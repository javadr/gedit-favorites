#!/bin/bash

VERSION="2024-05-01"

# Favorites archive build
# ======================

# Dossiers
CDIR="$( cd "$( dirname "$0" )" && pwd )"
BDIR=$CDIR"/build/"
SDIR=$CDIR"/src/"
DDIR=$CDIR"/docs/"

# Variables
APP_NAME='favorites'
BUILD_NAME='gedit-'$APP_NAME

echo "Build de "$APP_NAME

vname=$BUILD_NAME"-"$VERSION
archname=$vname".zip"
vdir=$BDIR$vname"/"
latest=$BDIR"/latest.zip"

# création du dossier de build dédié
mkdir $vdir

# Supprimer l'archive si elle existe déjà
if [[ -f $archname ]]; then
    rm $archname
fi
if [[ -f $latest ]]; then
    rm $latest
fi

# copie du fichier plugin
cp -p -t $vdir $SDIR$APP_NAME".plugin"

# traitement du dossier plugin
cp -prL -t $vdir $SDIR$APP_NAME

# Supprimer les fichier *.pyc, dossiers __pycache__
find $vdir -type f -name "*.pyc" -delete
find $vdir$APP_NAME -type d -name "__pycache__" -delete

# Création de l'archive tar.gz
cd $BDIR
zip -qr $archname $vname
cp -p $archname "latest.zip"

# Suppression du dossier de build
rm -fr $vdir
