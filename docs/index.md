---
title:      Gedit : Favorites
banner:     /lab/gedit-favorites/pics/2021-10-28.png
date:       2014-04-27
updated:    2023-12-04
cats:       [informatique]
tags:       [gedit, plugin, bookmark]
techs:      [python, gtk]
status:     in-process
version:    2023-12-03
source:     https://framagit.org/rui.nibau/gedit-favorites/
issues:     https://framagit.org/rui.nibau/gedit-favorites/-/issues
download:   /lab/gedit-favorites/build/latest.zip
contact:    true
itemtype:   SoftwareApplication
pagesAsSnippets: true
intro: Favorites est un plugin pour Gedit ≥ 45 permettant de maintenir une liste de fichiers favoris.
---

°°|about°°
## Présentation

Le plugin ''Favorites'' est né durant la migration sous Gnome 3.12 et que les changements tant internes qu'externes de Gedit ont provoqué la désactivation de certains plugins, [Gedit Favorites](https://github.com/Quixotix/gedit-favorites) notamment. J'avais la possibilité de patcher le code comme lors du [passage à la version 3.8](/notes/2013-07-06). Les modifications étaient cependant cette fois-ci plus importantes ; j'ai donc préféré réécrire le plugin en modifiant son interface pour mieux correspondre à mes usages.

À l'origine, le plugin permettait d'organiser des favoris sous forme d'arbre arborescent. Le code a cependant planté à partir de Gnome 40. Il a donc été décidé à partir de la version ``2021-10-28`` de remplacer cette gestion des favoris par une simple liste de fichier :

1. Pour alléger le code à gérer : je n'ai plus autant de temps à consacrer à la maintenance des plugins Gedit.
2. Pour répondre aux besoins utilisateurs — c'est-à-dire les miens :-) : je n'ai pas / plus l'usage d'une liste hiérarchique de favoris ; une liste plate suffit.

Les anciens favoris sont récupérés lors de la première installation de cette nouvelle version.

°°msg msg-info°°
Les personnes qui souhaitent continuer à utiliser la liste hiérarchique peuvent installer [la version ``2020-05-01``](/lab/gedit-favorites/build/gedit-favorites-2020-05-01.tar.gz) et corriger les bugs qui s'y trouvent.

°°|install°°
## Installation

..include::/data/snippets/gedit-plugin-install.md
    plugin-name = favorites

°°|usage°°
## Utilisation

°°pic pos-left sz-third°°
![Image](/lab/gedit-favorites/pics/2021-10-28_panel.jpg)

• Le panneau « Favorites » se présente sous la forme d'une barre d'outils et d'une liste de fichiers favoris.
• Le bouton  « - » permet de supprimer le favoris sélectionné.
• Le bouton « + » permet d'ajouter le fichier affiché en tant que favoris.
• Le nom des fichiers est modifiable en le sélectionnant.
• Les favoris sont enregistrés dans le fichier ``~/.config/gedit/favorites.json``.

°°|issues°°
## Bugs et évolutions

Voir [Les tickets du projet](https://framagit.org/rui.nibau/gedit-favorites/-/issues)

°°|history°°
## Historique

..include::./changelog.md

°°|res-ref°°
## ressources et références

..include::/data/snippets/gedit-plugin-ref.md

°°|licence°°
## Licence

..include::./licence.md

