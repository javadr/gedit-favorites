# Gedit favorites

Plugin for Gedit to manage favorite files.

* Website : https://omacronides.com/projets/gedit-favorites/
* Source : https://framagit.org/rui.nibau/gedit-favorites/
* Issues : https://framagit.org/rui.nibau/gedit-favorites/-/issues
